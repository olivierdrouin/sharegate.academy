﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sharegate.Academy.Task;

namespace Sharegate.Academy.Tests.Task
{
    /// <summary>
    /// Test the behavior of the TaskEdit class.
    /// </summary>
    [TestClass]
    public class TaskEditTests
    {
        #region Fields

        private TaskEdit _taskEdit;

        #endregion

        #region Setup

        /// <summary>
        /// Setup the test cases.
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            this._taskEdit = TaskEdit.New();
        }

        #endregion

        #region FromEntity

        /// <summary>
        /// Validates that the created edit values are the same than the entity.
        /// </summary>
        [TestMethod]
        public void FromEntity_KeepAllValuesFromEntity()
        {
            // Arrange
            var task = new Academy.Models.Entities.Task();
            task.Title = "Some Title";
            task.CategoryId = 123;
            task.Id = 111;

            // Act
            var result = TaskEdit.FromEntity(task);

            // Assert
            Assert.AreEqual(task.Title, result.Title);
            Assert.AreEqual(task.CategoryId, result.CategoryId);
            Assert.AreEqual(task.Id, result.Id);
        }

        #endregion

        #region ToEntity

        /// <summary>
        /// Validates that the created entity values are the same than the edit.
        /// </summary>
        [TestMethod]
        public void ToEntity_TransferAllValuesToEntity()
        {
            // Arrange
            this._taskEdit.Title = "Some Title";
            this._taskEdit.CategoryId = 123;
            this._taskEdit.Id = 111;

            // Act
            var result = this._taskEdit.ToEntity();

            // Assert
            Assert.AreEqual(this._taskEdit.Title, result.Title);
            Assert.AreEqual(this._taskEdit.CategoryId, result.CategoryId);
            Assert.AreEqual(this._taskEdit.Id, result.Id);
        }

        #endregion

        #region IsValid

        /// <summary>
        /// Validates that if all rules are respected, returns true
        /// </summary>
        [TestMethod]
        public void IsValid_IfAllValuesAreValid_ReturnsTrue()
        {
            // Arrange
            this._taskEdit.Title = "ValidTitle";
            this._taskEdit.CategoryId = 0;

            // Act
            var result = this._taskEdit.IsValid;

            // Assert
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Validates that if title is empty, returns false
        /// </summary>
        [TestMethod]
        public void IsValid_IfTitleIsEmpty_ReturnsFalse()
        {
            // Arrange
            this._taskEdit.Title = string.Empty;
            this._taskEdit.CategoryId = 0;

            // Act
            var result = this._taskEdit.IsValid;

            // Assert
            Assert.IsFalse(result);
        }
        
        [TestMethod]
        public void IsValid_IfTitleIsWhiteSpace_ReturnsFalse()
        {
            // Arrange
            this._taskEdit.Title = " ";
            this._taskEdit.CategoryId = 0;

            // Act
            var result = this._taskEdit.IsValid;

            // Assert
            Assert.IsFalse(result);
        }
        
        [TestMethod]
        public void IsValid_IfTitleStartsWithSpace_ReturnsFalse()
        {
            // Arrange
            this._taskEdit.Title = " Test";
            this._taskEdit.CategoryId = 0;

            // Act
            var result = this._taskEdit.IsValid;

            // Assert
            Assert.IsFalse(result);
        }
        
        [TestMethod]
        public void IsValid_IfTitleEndsWithSpace_ReturnsFalse()
        {
            // Arrange
            this._taskEdit.Title = "Test ";
            this._taskEdit.CategoryId = 0;

            // Act
            var result = this._taskEdit.IsValid;

            // Assert
            Assert.IsFalse(result);
        }

        /// <summary>
        /// Validates that if category is not valid, returns false
        /// </summary>
        [TestMethod]
        public void IsValid_IfCategoryIsNotValid_ReturnsFalse()
        {
            // Arrange
            this._taskEdit.Title = "ValidTitle";
            this._taskEdit.CategoryId = -1;

            // Act
            var result = this._taskEdit.IsValid;

            // Assert
            Assert.IsFalse(result);
        }

        #endregion
    }
}
