﻿using System;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Sharegate.Academy.Models.Entities;
using Sharegate.Academy.Models.Repositories;

namespace Sharegate.Academy.Tests.Models.Repositories
{
    /// <summary>
    /// Test the behavior of the XmlTaskRepository class.
    /// </summary>
    [TestClass]
    [DeploymentItem("Repository.xml")]
    public class XmlTaskRepositoryTests
    {
        #region Fields

        private XmlTaskRepository _repository;

        #endregion

        #region Setup

        /// <summary>
        /// Setup the test cases.
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            this._repository = new XmlTaskRepository();
        }

        #endregion

        #region GetCategories

        /// <summary>
        /// Validates that GetCategories always returns all the available categories.
        /// </summary>
        [TestMethod]
        public void GetCategories_Always_ReturnsAllCategories()
        {
            // Act
            var results = this._repository.GetCategories();

            // Assert
            Assert.AreEqual(3, results.Count());
        }

        #endregion
        
        #region AddTask
        
        [TestMethod]
        [DeploymentItem("Repository.xml", "AddTaskTest")]
        public void AddTask_WhenNewTask_AddsTask()
        {
            // Arrange
            
            this._repository = new XmlTaskRepository("AddTaskTest/Repository.xml");
            var task = new Academy.Models.Entities.Task(99, "Test Task", 201) { IsCompleted = true, };
            
            // Act
            this._repository.AddTask(task);

            // Assert
            var tasks = this._repository.GetTasks();
            var returnedTask = tasks.FirstOrDefault(x => x.Id == task.Id);
            
            Assert.IsNotNull(returnedTask);

            Assert.AreEqual(task.Title, returnedTask.Title);
            Assert.AreEqual(task.CategoryId, returnedTask.CategoryId);
            Assert.AreEqual(task.IsCompleted, returnedTask.IsCompleted);
        }
        
        #endregion
        
        #region UpdateTask
        
        [TestMethod]
        [DeploymentItem("Repository.xml", "UpdateTask")]
        public void UpdateTask_WhenExistingTask_UpdatesTask()
        {
            // Arrange
            
            this._repository = new XmlTaskRepository("UpdateTask/Repository.xml");
            var task = new Academy.Models.Entities.Task(1, "Test Task", 201) { IsCompleted = true, };

            // Act
            this._repository.UpdateTask(task);

            // Assert
            var tasks = this._repository.GetTasks();
            var returnedTask = tasks.FirstOrDefault(x => x.Id == task.Id);
            
            Assert.IsNotNull(returnedTask);

            Assert.AreEqual(task.Title, returnedTask.Title);
            Assert.AreEqual(task.CategoryId, returnedTask.CategoryId);
            Assert.AreEqual(task.IsCompleted, returnedTask.IsCompleted);
        }
        
        #endregion

        #region AddCategory

        /// <summary>
        /// Validates that AddCategory always appends a category to the XML.
        /// </summary>
        [TestMethod]
        [DeploymentItem("Repository.xml", "AddCategoryTest")]
        public void AddCategory_Always_AddsCategory()
        {
            // Arrange
            this._repository = new XmlTaskRepository("AddCategoryTest/Repository.xml");
            var random = new Random().Next();
            var count = this._repository.GetCategories().Count();

            // Act
            this._repository.AddCategory(new Category { Id = random, Title = $"New Category {random}" });

            // Assert
            Assert.AreEqual(count + 1, this._repository.GetCategories().Count());
        }

        #endregion

        #region GetTasks

        /// <summary>
        /// Validates that GetTasks always returns all the available tasks.
        /// </summary>
        [TestMethod]
        public void GetTasks_Always_ReturnsAllTasks()
        {
            // Act
            var results = this._repository.GetTasks();

            // Assert
            Assert.AreEqual(10, results.Count());
        }

        #endregion
    }
}
