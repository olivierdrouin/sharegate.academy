﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Sharegate.Academy.Models.Entities;
using Sharegate.Academy.Models.Repositories;
using Sharegate.Academy.Options;

namespace Sharegate.Academy.Tests.Models.Repositories
{
    [TestClass]
    public class SharePointTaskRepositoryTests
    {
        #region Fields

        private const string WebUrl = "http://gamedev-2016/sites/olidest/SharegateAcademyTests/";
        
        private ITaskRepository _repository;
        private IOptionsService _options;

        #endregion

        #region Setup

        [TestInitialize()]
        public void Setup()
        {
            // Register Unity Instances
            this._options = new OptionsService();
            this._options.CurrentOptions.WebUrl = WebUrl;
            this._options.CurrentOptions.CategoryListTitle = "AcademyCategories";
            this._options.CurrentOptions.TaskListTitle = "AcademyTasks";
            this._options.CurrentOptions.RepositoryType = RepositoryType.SharePoint;

            // Set our test instance.
            this._repository = new SharePointTaskRepository(this._options);
        }

        #endregion

        #region GetCategories

        [TestMethod]
        public void GetCategories_Always_ReturnsAllCategories()
        {
            // ARRANGE
            this.ClearAll();

            // Add the categories.
            this._repository.AddCategory(new Category("Star Wars"));
            this._repository.AddCategory(new Category("Star Trek"));
            this._repository.AddCategory(new Category("Star Rekt"));

            // ACT
            var categories = this._repository.GetCategories().ToList();

            // ASSERT
            Assert.AreEqual(3, categories.Count());
            Assert.IsTrue(categories.Any(x => x.Title == "Star Wars"));
            Assert.IsTrue(categories.Any(x => x.Title == "Star Trek"));
            Assert.IsTrue(categories.Any(x => x.Title == "Star Rekt"));
        }

        #endregion

        #region GetTasks

        [TestMethod]
        public void GetTasks_Always_ReturnsAllTasks()
        {
            // ARRANGE
            this.ClearAll();

            // Add a category that will be referenced by the created tasks.
            this._repository.AddCategory(new Category("Star Wars"));
            var categories = this._repository.GetCategories().ToList();
            var starWarsId = categories.Single(x => x.Title == "Star Wars").Id;

            // Add the tasks.
            this._repository.AddTask(new Academy.Models.Entities.Task(1, "A New Hope", starWarsId));
            this._repository.AddTask(new Academy.Models.Entities.Task(2, "Empire Strikes Back", starWarsId));
            this._repository.AddTask(new Academy.Models.Entities.Task(3, "Return of the Jedi", starWarsId));

            // ACT
            var tasks = this._repository.GetTasks().ToList();

            // ASSERT
            Assert.AreEqual(3, tasks.Count);

            Assert.IsTrue(tasks.Any(x => x.Title == "A New Hope"));
            Assert.IsTrue(tasks.Any(x => x.Title == "Empire Strikes Back"));
            Assert.IsTrue(tasks.Any(x => x.Title == "Return of the Jedi"));
        }

        #endregion

        #region AddCategory

        [TestMethod]
        public void AddCategory_Always_CreatesCategory()
        {
            // ARRANGE
            var categoryTitle = Guid.NewGuid().ToString("N");

            // ACT
            this._repository.AddCategory(new Category(categoryTitle));

            // ASSERT
            var categories = this._repository.GetCategories();

            Assert.IsTrue(categories.Any(x => x.Title == categoryTitle));
        }

        #endregion

        #region DeleteAllCategories

        [TestMethod]
        public void DeleteAllCategories_Always_ClearsAllCategories()
        {
            // ARRANGE
            this._repository.AddCategory(new Category(Guid.NewGuid().ToString("N")));
            this._repository.AddCategory(new Category(Guid.NewGuid().ToString("N")));
            this._repository.AddCategory(new Category(Guid.NewGuid().ToString("N")));

            // ACT
            this._repository.DeleteAllCategories();

            // ASSERT
            var categories = this._repository.GetCategories();

            Assert.AreEqual(0, categories.Count());
        }

        #endregion

        #region AddTask

        [TestMethod]
        public void AddTask_Always_CreatesTask()
        {
            // ARRANGE
            var categoryTitle = Guid.NewGuid().ToString("N");
            this._repository.AddCategory(new Category(categoryTitle));
            var categoryId = this._repository.GetCategories().Single(x => x.Title == categoryTitle).Id;

            var taskTitle = Guid.NewGuid().ToString("N");

            // ACT
            this._repository.AddTask(new Academy.Models.Entities.Task(1, taskTitle, categoryId));

            // ASSERT
            var tasks = this._repository.GetTasks();

            var task = tasks.FirstOrDefault(x => x.Title == taskTitle);
            Assert.IsNotNull(task);
            Assert.AreEqual(taskTitle, task.Title);
            Assert.AreEqual(categoryId, task.CategoryId);
        }

        #endregion

        #region UpdateTask

        [TestMethod]
        public void UpdateTask_Always_UpdatesTask()
        {
            // ARRANGE
            var category1Title = Guid.NewGuid().ToString("N");
            var category2Title = Guid.NewGuid().ToString("N");

            // Create two categories. One for the initial creation and a second for the update.
            this._repository.AddCategory(new Category(category1Title));
            this._repository.AddCategory(new Category(category2Title));

            var categories = this._repository.GetCategories().ToList();
            var category1Id = categories.Single(x => x.Title == category1Title).Id;
            var category2Id = categories.Single(x => x.Title == category2Title).Id;

            // Create a task that will be updated.
            var taskTitle = Guid.NewGuid().ToString("N");
            this._repository.AddTask(new Academy.Models.Entities.Task(1, taskTitle, category1Id));

            var tasks = this._repository.GetTasks();

            // Get the previously created task.
            var createdTask = tasks.FirstOrDefault(x => x.Title == taskTitle);
            Assert.IsNotNull(createdTask);

            // Update the task.
            var newTaskTitle = Guid.NewGuid().ToString("N");
            createdTask.Title = newTaskTitle;
            createdTask.CategoryId = category2Id;

            // ACT
            this._repository.UpdateTask(createdTask);

            // ASSERT
            tasks = this._repository.GetTasks();

            var task = tasks.FirstOrDefault(x => x.Id == createdTask.Id);
            Assert.IsNotNull(task);
            Assert.AreEqual(newTaskTitle, task.Title);
            Assert.AreEqual(category2Id, task.CategoryId);
        }

        #endregion

        #region DeleteAllTasks

        [TestMethod]
        public void DeleteAllTasks_Always_ClearsAllTasks()
        {
            // ARRANGE
            var categoryTitle = Guid.NewGuid().ToString("N");

            // Create a category to reference in the tasks.
            this._repository.AddCategory(new Category(categoryTitle));
            var categoryId = this._repository.GetCategories().Single(x => x.Title == categoryTitle).Id;

            // Create some tasks that will be deleted.
            this._repository.AddTask(new Academy.Models.Entities.Task(1, Guid.NewGuid().ToString("N"), categoryId));
            this._repository.AddTask(new Academy.Models.Entities.Task(2, Guid.NewGuid().ToString("N"), categoryId));
            this._repository.AddTask(new Academy.Models.Entities.Task(3, Guid.NewGuid().ToString("N"), categoryId));

            // ACT
            this._repository.DeleteAllTasks();

            // ASSERT
            var tasks = this._repository.GetTasks();
            Assert.AreEqual(0, tasks.Count());
        }

        #endregion

        #region Methods

        private void ClearAll()
        {
            this._repository.DeleteAllTasks();
            this._repository.DeleteAllCategories();
        }

        #endregion
    }
}
