﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sharegate.Academy.Options;

namespace Sharegate.Academy.Tests.Options
{
    [TestClass]
    public class OptionsServiceTest
    {
        private UnityContainer _container;

        [TestInitialize]
        public void Setup()
        {
            this._container = new UnityContainer();
        }

        [TestMethod]
        public void OptionsService_WhenSettingsFileDoesNotExist_CreatesServiceWithDefaultOptions()
        {
            // Arrange
            if (File.Exists(OptionsService.FilePath))
            {
                File.Delete(OptionsService.FilePath);
            }

            // Act
            var service = this._container.Resolve<OptionsService>();

            // Assert
            Assert.IsNotNull(service);
            Assert.IsNotNull(service.CurrentOptions);
        }

        [TestMethod]
        public void Save_Always_PreservesSettingsOnNextInitialization()
        {
            // Arrange
            const string Category = "cat";
            const string Site = "site";
            const string Task = "task";

            var service = this._container.Resolve<OptionsService>();
            service.CurrentOptions.CategoryListTitle = Category;
            service.CurrentOptions.WebUrl = Site;
            service.CurrentOptions.TaskListTitle = Task;
            
            // Act
            service.Save();

            // Assert
            service = this._container.Resolve<OptionsService>();
            Assert.AreEqual(service.CurrentOptions.CategoryListTitle, Category);
            Assert.AreEqual(service.CurrentOptions.WebUrl, Site);
            Assert.AreEqual(service.CurrentOptions.TaskListTitle, Task);

        }
    }
}
