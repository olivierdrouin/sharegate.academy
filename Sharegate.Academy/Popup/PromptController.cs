﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Practices.Unity;

using Prism.Regions;

namespace Sharegate.Academy.Popup
{
    /// <summary>
    /// Controller to prompt errors to the user.
    /// </summary>
    public class PromptController : IPromptController
    {
        private readonly IRegionManager _regionManager;

        private readonly IUnityContainer _container;

        /// <summary>
        /// Initializes a new instance of the <see cref="PromptController" /> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="regionManager">The region manager.</param>
        public PromptController(IUnityContainer container, IRegionManager regionManager)
        {
            this._container = container;
            this._regionManager = regionManager;
        }

        /// <summary>
        /// Shows the error.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public void ShowError(Exception exception)
        {
            var popup = this._container.Resolve<ErrorView>();
            popup.ViewModel.Initialize(exception);
            this.ShowPopup(popup);
        }

        private void ShowPopup(object view)
        {
            var region = this._regionManager.Regions[RegionNames.Popup];
            if (!region.Views.Contains(view))
            {
                region.Add(view);
            }

            region.Activate(view);
        }
    }
}
