﻿using System;

namespace Sharegate.Academy.Popup
{
    /// <summary>
    /// The base interface for the prompt controller.
    /// </summary>
    public interface IPromptController
    {
        /// <summary>
        /// Shows the error.
        /// </summary>
        /// <param name="exception">The exception.</param>
        void ShowError(Exception exception);
    }
}
