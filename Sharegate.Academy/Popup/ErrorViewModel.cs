﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;

namespace Sharegate.Academy.Popup
{
    /// <summary>
    /// The view model for the error view.
    /// </summary>
    /// <seealso cref="Prism.Mvvm.BindableBase" />
    public class ErrorViewModel : BindableBase
    {
        #region Fields

        private readonly IRegionManager _regionManager;

        private string _title;

        private string _description;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorViewModel"/> class.
        /// </summary>
        /// <param name="regionManager">The region manager.</param>
        public ErrorViewModel(IRegionManager regionManager)
        {
            this._regionManager = regionManager;
            this.CloseCommand = new DelegateCommand(this.Close);
        }

        #endregion

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title
        {
            get { return this._title; }
            set { this.SetProperty(ref this._title, value); }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description
        {
            get { return this._description; }
            set { this.SetProperty(ref this._description, value); }
        }

        /// <summary>
        /// Gets the close command.
        /// </summary>
        public ICommand CloseCommand { get; private set; }

        #region Methods

        /// <summary>
        /// Initializes the view model with the given exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public void Initialize(Exception ex)
        {
            this.Title = "An error occurred";
            this.Description = ex.Message;
        }

        private void Close()
        {
            var region = this._regionManager.Regions[RegionNames.Popup];

            foreach (var view in region.Views)
            {
                region.Remove(view);
            }
        }

        #endregion
    }
}
