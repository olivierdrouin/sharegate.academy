﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sharegate.Academy.Popup
{
    /// <summary>
    /// Interaction logic for ErrorView.xaml
    /// </summary>
    public partial class ErrorView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorView"/> class.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        public ErrorView(ErrorViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        public ErrorViewModel ViewModel
        {
            get { return (ErrorViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }
    }
}
