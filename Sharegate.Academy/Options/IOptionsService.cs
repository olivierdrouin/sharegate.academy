namespace Sharegate.Academy.Options
{
    /// <summary>
    /// The interface for the options service.
    /// </summary>
    public interface IOptionsService
    {
        /// <summary>
        /// Gets or sets the current options.
        /// </summary>
        ApplicationOptions CurrentOptions { get; set; }

        /// <summary>
        /// Save the current configuration to disk
        /// </summary>
        void Save();
    }
}