﻿using Prism.Modularity;
using Prism.Regions;

namespace Sharegate.Academy.Options
{
    /// <summary>
    /// The options module.
    /// </summary>
    /// <seealso cref="IModule" />
    public class OptionsModule : IModule
    {
        private readonly IRegionManager _regionManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsModule" /> class.
        /// </summary>
        /// <param name="regionManager">The region manager.</param>
        public OptionsModule(IRegionManager regionManager)
        {
            this._regionManager = regionManager;
        }

        /// <summary>
        /// Notifies the module that it has be initialized.
        /// </summary>git
        public void Initialize()
        {
            this._regionManager.RegisterViewWithRegion(RegionNames.Content, typeof(OptionsView));
        }
    }
}
