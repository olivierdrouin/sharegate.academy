﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Sharegate.Academy.Options
{
    /// <summary>
    /// Represents the options for the application.
    /// </summary>
    public class ApplicationOptions
    {
        /// <summary>
        /// Gets or sets the site URL.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Justification = "For serialization purposes")]
        public string WebUrl { get; set; }

        /// <summary>
        /// Gets or sets the category list title.
        /// </summary>
        public string CategoryListTitle { get; set; }

        /// <summary>
        /// Gets or sets the task list title.
        /// </summary>
        public string TaskListTitle { get; set; }

        /// <summary>
        /// Gets or sets the type of the repository.
        /// </summary>
        /// <value>
        /// The type of the repository.
        /// </value>
        public RepositoryType RepositoryType { get; set; }
    }
}
