﻿using System;
using System.IO;
using System.Xml;

namespace Sharegate.Academy.Options
{
    /// <summary>
    /// The service that contains application options.
    /// </summary>
    public class OptionsService : IOptionsService
    {
        /// <summary>
        /// The file path
        /// </summary>
        public const string FilePath = "Options.xml";
        
        private readonly System.Xml.Serialization.XmlSerializer _serializer = new System.Xml.Serialization.XmlSerializer(typeof(ApplicationOptions));

        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsService"/> class.
        /// </summary>
        public OptionsService()
        {
            if (!File.Exists(FilePath))
            {
                var options = new ApplicationOptions();
                options.WebUrl = "http://gamedev-2016/sites/olidest/SharegateAcademy";
                options.CategoryListTitle = "AcademyCategories";
                options.TaskListTitle = "AcademyTasks";
                options.RepositoryType = RepositoryType.Filesystem;
                this.CurrentOptions = options;
                this.Save();
            }
            else
            {
                this.Load();
            }
           
            var text = File.ReadAllText(FilePath);
            var document = new XmlDocument();
            document.LoadXml(text);
        }

        /// <summary>
        /// Gets or sets the current options.
        /// </summary>
        public ApplicationOptions CurrentOptions { get; set; }

        /// <summary>
        /// Save the current configuration to disk
        /// </summary>
        public void Save()
        {
            using (var file = File.Create(FilePath))
            {
                this._serializer.Serialize(file, this.CurrentOptions);
                file.Flush();
            }
        }

        private void Load()
        {
            using (var file = File.OpenRead(FilePath))
            {
                this.CurrentOptions = (ApplicationOptions)this._serializer.Deserialize(file);
            }
        }
    }
}
