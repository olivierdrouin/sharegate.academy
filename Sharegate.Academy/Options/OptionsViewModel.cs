﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Windows.Input;

using Microsoft.Practices.Unity;

using Prism.Commands;
using Prism.Mvvm;
using Sharegate.Academy.Models.Entities;
using Sharegate.Academy.Models.Repositories;
using Sharegate.Academy.Navigation;

namespace Sharegate.Academy.Options
{
    /// <summary>
    /// The options view model.
    /// </summary>
    /// <seealso cref="BindableBase" />
    public class OptionsViewModel : BindableBase
    {
        #region Fields

        private readonly IUnityContainer _container;
        private readonly ITaskRepository _taskRepository;
        private readonly IOptionsService _optionsService;
        private readonly INavigationController _navigationController;
        private RepositoryType _repositoryType;

        private string _categoryName;
        private string _webUrl;
        private string _taskList;
        private string _categoryList;
        private bool _repositoryTypeChanged;

        #endregion

        #region Constructor

        public OptionsViewModel(IUnityContainer container, ITaskRepository taskRepository, IOptionsService optionsService, INavigationController navigationController)
        {
            this._container = container;
            this._taskRepository = taskRepository;
            this._optionsService = optionsService;
            this._navigationController = navigationController;

            this.CancelCommand = new DelegateCommand(this.Cancel);
            this.SaveCommand = new DelegateCommand(this.Save);
            this.AddCategoryCommand = new DelegateCommand(this.AddCategory, this.CanExecuteAddCategory);

            this.Categories.AddRange(this._taskRepository.GetCategories());

            // Load the options
            this._repositoryType = this._optionsService.CurrentOptions.RepositoryType;
            this._webUrl = this._optionsService.CurrentOptions.WebUrl;
            this._categoryList = this._optionsService.CurrentOptions.CategoryListTitle;
            this._taskList = this._optionsService.CurrentOptions.TaskListTitle;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the category.
        /// </summary>
        public string CategoryName
        {
            get { return this._categoryName; }
            set { this.SetProperty(ref this._categoryName, value); }
        }

        /// <summary>
        /// Gets or sets the site URL.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Justification = "For serialization purposes")]
        public string WebUrl
        {
            get { return this._webUrl; }
            set { this.SetProperty(ref this._webUrl, value); }
        }

        /// <summary>
        /// Gets or sets the task list.
        /// </summary>
        public string TaskList
        {
            get { return this._taskList; }
            set { this.SetProperty(ref this._taskList, value); }
        }

        /// <summary>
        /// Gets or sets the category list.
        /// </summary>
        public string CategoryList
        {
            get { return this._categoryList; }
            set { this.SetProperty(ref this._categoryList, value); }
        }
        
        public bool RepositoryTypeChanged
        {
            get { return this._repositoryTypeChanged; }
            set { this.SetProperty(ref this._repositoryTypeChanged, value); }
        }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        public ObservableCollection<object> Categories { get; } = new ObservableCollection<object>();

        /// <summary>
        /// Gets or sets the type of the repository.
        /// </summary>
        public RepositoryType RepositoryType
        {
            get { return this._repositoryType; }
            set { this.SetProperty(ref this._repositoryType, value); }
        }

        /// <summary>
        /// Gets or sets the cancel command.
        /// </summary>
        public ICommand CancelCommand { get; set; }

        /// <summary>
        /// Gets or sets the save command.
        /// </summary>
        public ICommand SaveCommand { get; set; }

        /// <summary>
        /// Gets or sets the add category command.
        /// </summary>
        public DelegateCommand AddCategoryCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Called when one of the model's properties changes.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == nameof(this.RepositoryType))
            {
                this.RepositoryTypeChanged = this.RepositoryType != this._optionsService.CurrentOptions.RepositoryType;
            }

            this.AddCategoryCommand.RaiseCanExecuteChanged();
        }

        private void Cancel()
        {
            this._navigationController.ShowTasks(0);
        }

        private void Save()
        {
            this._optionsService.CurrentOptions.RepositoryType = this.RepositoryType;
            this.RepositoryTypeChanged = false;
            this.RegisterTaskRepository();
            
            this._optionsService.CurrentOptions.CategoryListTitle = this.CategoryList;
            this._optionsService.CurrentOptions.WebUrl = this.WebUrl;
            this._optionsService.CurrentOptions.TaskListTitle = this.TaskList;
            this._optionsService.Save();
            this._navigationController.RefreshNavigation();
            this._navigationController.ShowTasks(0);
        }

        private void RegisterTaskRepository()
        {
            Bootstrapper.RegisterTaskRepository(this._container, this.RepositoryType);
        }

        private void AddCategory()
        {
            var category = new Category { Title = this.CategoryName };
            this._taskRepository.AddCategory(category);
            this.Categories.Add(category);

            this.CategoryName = string.Empty;
            this._navigationController.RefreshNavigation();
        }

        private bool CanExecuteAddCategory()
        {
            return !string.IsNullOrWhiteSpace(this.CategoryName);
        }

        #endregion
    }
}
