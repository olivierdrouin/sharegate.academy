﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sharegate.Academy.Options
{
    /// <summary>
    /// Interaction logic for OptionsView.xaml
    /// </summary>
    public partial class OptionsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsView"/> class.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        public OptionsView(OptionsViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        /// <value>
        /// The view model.
        /// </value>
        public OptionsViewModel ViewModel
        {
            get { return (OptionsViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }
    }
}
