﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Unity;
using Prism.Commands;
using Prism.Mvvm;
using Sharegate.Academy.Async;
using Sharegate.Academy.Models.Entities;
using Sharegate.Academy.Models.Repositories;

namespace Sharegate.Academy.Navigation
{
    /// <summary>
    /// Navigation view model
    /// </summary>
    /// <seealso cref="Prism.Mvvm.BindableBase" />
    public class NavigationViewModel : BindableBase
    {
        #region Fields

        private readonly INavigationController _navigationController;
        private readonly IAsyncHelper _asyncHelper;
        private readonly IUnityContainer _container;
        private bool _isLoading;
        private ITaskRepository _taskRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationViewModel" /> class.
        /// </summary>
        /// <param name="taskRepository">The task repository.</param>
        /// <param name="navigationController">The navigation controller.</param>
        /// <param name="asyncHelper">The asynchronous helper.</param>
        /// <param name="container">Unity container</param>
        public NavigationViewModel(ITaskRepository taskRepository, INavigationController navigationController, IAsyncHelper asyncHelper, IUnityContainer container)
        {
            this._taskRepository = taskRepository;
            this._navigationController = navigationController;
            this._asyncHelper = asyncHelper;
            this._container = container;
            this.CategoryList = new ObservableCollection<Category>();
            this.CategorySelectedCommand = new DelegateCommand<Category>(this.CategorySelected);
            this.RefreshCommand = new DelegateCommand(this.RefreshList);
            this.OpenSettingsCommand = new DelegateCommand(this.OpenSettings);

            this.CategoryList.AddRange(this._taskRepository.GetCategories());
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the category list.
        /// </summary>
        public ObservableCollection<Category> CategoryList { get; }

        /// <summary>
        /// Gets or sets the open settings command.
        /// </summary>
        public ICommand OpenSettingsCommand { get; set; }

        /// <summary>
        /// Gets or sets the category selected command.
        /// </summary>
        public ICommand CategorySelectedCommand { get; set; }

        /// <summary>
        /// Gets or sets the refresh command.
        /// </summary>
        public ICommand RefreshCommand { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading.
        /// </summary>
        public bool IsLoading
        {
            get { return this._isLoading; }
            set { this.SetProperty(ref this._isLoading, value); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refreshes the repository.
        /// </summary>
        public void RefreshRepository()
        {
            this._taskRepository = this._container.Resolve<ITaskRepository>();
            this.RefreshList();
        }

        private void CategorySelected(Category cat)
        {
            this._navigationController.ShowTasks(cat.Id);
        }

        private void RefreshList()
        {
            this.IsLoading = true;

            this._asyncHelper.Run(
                () => this._taskRepository.GetCategories(),
                x =>
                {
                    if (x.Success)
                    {
                        this.CategoryList.Clear();
                        this.CategoryList.AddRange(x.Result);
                    }
                    
                     this.IsLoading = false;
                });
        }
        
        private void OpenSettings()
        {
            this._navigationController.ShowOptions();
        }

        #endregion
    }
}
