﻿using Microsoft.Practices.Unity;

using Prism.Modularity;
using Prism.Regions;

namespace Sharegate.Academy.Navigation
{
    /// <summary>
    /// The navigation module for the application.
    /// </summary>
    /// <seealso cref="IModule" />
    public class NavigationModule : IModule
    {
        private readonly IRegionManager _regionManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationModule"/> class.
        /// </summary>
        /// <param name="regionManager">The region manager.</param>
        public NavigationModule(IRegionManager regionManager)
        {
            this._regionManager = regionManager;
        }

        /// <summary>
        /// Notifies the module that it has be initialized.
        /// </summary>
        public void Initialize()
        {
            this._regionManager.RegisterViewWithRegion(RegionNames.Navigation, typeof(NavigationView));
        }
    }
}
