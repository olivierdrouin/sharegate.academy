﻿namespace Sharegate.Academy.Navigation
{
    /// <summary>
    /// The interface for the navigation controller.
    /// </summary>
    public interface INavigationController
    {
        /// <summary>
        /// Shows the tasks.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        void ShowTasks(int categoryId);

        void ShowOptions();

        /// <summary>
        /// Refreshes the navigation screen.
        /// </summary>
        void RefreshNavigation();
    }
}