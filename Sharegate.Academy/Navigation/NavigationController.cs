﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Practices.Unity;

using Prism.Regions;

using Sharegate.Academy.Options;
using Sharegate.Academy.Popup;
using Sharegate.Academy.Task;

namespace Sharegate.Academy.Navigation
{
    /// <summary>
    /// The navigation controller for the application.
    /// </summary>
    /// <seealso cref="INavigationController" />
    public class NavigationController : INavigationController
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationController"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="regionManager">The region manager.</param>
        public NavigationController(IUnityContainer container, IRegionManager regionManager)
        {
            this._container = container;
            this._regionManager = regionManager;
        }

        /// <summary>
        /// Shows the tasks.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        public void ShowTasks(int categoryId)
        {
            var view = this._container.Resolve<TaskView>();
            view.ViewModel.Initialize(categoryId);
            this.ShowViewInRegion(view, RegionNames.Content);
        }
        
        public void ShowOptions()
        {
            var view = this._container.Resolve<OptionsView>();
            this.ShowViewInRegion(view, RegionNames.Content);
        }

        /// <summary>
        /// Refreshes the navigation repository.
        /// </summary>
        public void RefreshNavigation()
        {
            var navigationView = this._regionManager.Regions[RegionNames.Navigation].Views.OfType<NavigationView>().First();
            navigationView.ViewModel.RefreshRepository();
        }

        private void ShowViewInRegion(object view, string regionName)
        {
            var region = this._regionManager.Regions[regionName];
            if (!region.Views.Contains(view))
            {
                region.Add(view);
            }

            region.Activate(view);
        }
    }
}
