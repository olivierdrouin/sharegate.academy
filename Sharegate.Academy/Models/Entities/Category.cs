﻿namespace Sharegate.Academy.Models.Entities
{
    /// <summary>
    /// Model class for task categories.
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Category"/> class.
        /// </summary>
        public Category()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Category"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        public Category(string title)
            : this()
        {
            this.Title = title;
        }

        /// <summary>
        /// Gets or sets the category identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the category title.
        /// </summary>
        public string Title { get; set; }
    }
}
