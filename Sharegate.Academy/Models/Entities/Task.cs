﻿using System.Diagnostics.CodeAnalysis;

namespace Sharegate.Academy.Models.Entities
{
    /// <summary>
    /// Model class for a Task object.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Justification = "Reviewed.")]
    public class Task
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Task"/> class.
        /// </summary>
        public Task()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Task"/> class.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="title">The title.</param>
        /// <param name="categoryId">The category identifier.</param>
        public Task(int id, string title, int categoryId)
            : this()
        {
            this.Id = id;
            this.Title = title;
            this.CategoryId = categoryId;
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title of the task.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the task's category identifier.
        /// </summary>
        public int CategoryId { get; set; }
        
        public bool IsCompleted { get; set; }
    }
}
