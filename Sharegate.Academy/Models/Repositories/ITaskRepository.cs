﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Sharegate.Academy.Models.Entities;

namespace Sharegate.Academy.Models.Repositories
{
    /// <summary>
    /// Repository to fetch category and task data.
    /// </summary>
    public interface ITaskRepository
    {
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns>A collection of categories.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Reviewed.")]
        IEnumerable<Category> GetCategories();

        /// <summary>
        /// Adds the category.
        /// </summary>
        /// <param name="category">The category.</param>
        void AddCategory(Category category);

        /// <summary>
        /// Gets the tasks.
        /// </summary>
        /// <returns>A collection of tasks.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Reviewed.")]
        IEnumerable<Entities.Task> GetTasks();

        /// <summary>
        /// Adds the task.
        /// </summary>
        /// <param name="task">The task.</param>
        void AddTask(Entities.Task task);

        /// <summary>
        /// Updates the task.
        /// </summary>
        /// <param name="task">The task.</param>
        void UpdateTask(Entities.Task task);

        /// <summary>
        /// Deletes all categories.
        /// </summary>
        void DeleteAllCategories();

        /// <summary>
        /// Deletes all tasks.
        /// </summary>
        void DeleteAllTasks();
    }
}
