﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;

using Sharegate.Academy.Models.Entities;

namespace Sharegate.Academy.Models.Repositories
{
    /// <summary>
    /// Repository backed by an XML file on disk.
    /// </summary>
    public class XmlTaskRepository : ITaskRepository
    {
        public const string DefaultFilePath = "Repository.xml";

        private readonly string _filePath;

        public XmlTaskRepository()
            : this(DefaultFilePath)
        {
        }
        
        internal XmlTaskRepository(string filePath)
        {
            this._filePath = filePath;
        }
        
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns>The collection of categories.</returns>
        public IEnumerable<Category> GetCategories()
        {
            var text = File.ReadAllText(this._filePath);
            var document = new XmlDocument();
            document.LoadXml(text);

            var elements = document.SelectSingleNode("Entities");
            var group = elements?.SelectSingleNode("Categories");
            var items = group?.SelectNodes("Category");

            for (var i = 0; i < items?.Count; i++)
            {
                yield return new Category
                {
                    Id = int.Parse(items[i]?.Attributes?["Id"].InnerText),
                    Title = items[i]?.Attributes?["Title"].InnerText
                };
            }
        }

        /// <summary>
        /// Adds the category to the XML node list.
        /// </summary>
        /// <param name="category">The category.</param>
        public void AddCategory(Category category)
        {
            var text = File.ReadAllText(this._filePath);
            var document = new XmlDocument();
            document.LoadXml(text);

            var elements = document.SelectSingleNode("Entities");
            var group = elements?.SelectSingleNode("Categories");

            var element = document.CreateElement("Category");

            var id = document.CreateAttribute("Id");
            id.InnerText = category.Id.ToString(CultureInfo.InvariantCulture);
            element.Attributes.Append(id);

            var title = document.CreateAttribute("Title");
            title.InnerText = category.Title;
            element.Attributes.Append(title);

            group?.AppendChild(element);

            using (var stringWriter = new StringWriter(CultureInfo.InvariantCulture))
            {
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    document.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    File.WriteAllText(this._filePath, stringWriter.GetStringBuilder().ToString());
                }
            }
        }

        /// <summary>
        /// Gets the tasks.
        /// </summary>
        /// <returns>The collection of tasks.</returns>
        public IEnumerable<Entities.Task> GetTasks()
        {
            var text = File.ReadAllText(this._filePath);
            var document = new XmlDocument();
            document.LoadXml(text);

            var elements = document.SelectSingleNode("Entities");
            var group = elements?.SelectSingleNode("Tasks");
            var items = group?.SelectNodes("Task");

            for (var i = 0; i < items?.Count; i++)
            {
                yield return new Entities.Task
                {
                    Id = int.Parse(items[i]?.Attributes?["Id"].InnerText),
                    Title = items[i]?.Attributes?["Title"].InnerText,
                    CategoryId = int.Parse(items[i]?.Attributes?["CategoryId"].InnerText),
                    IsCompleted = bool.Parse(items[i]?.Attributes?["IsCompleted"].InnerText),
                };
            }
        }

        /// <summary>
        /// Adds the task.
        /// </summary>
        /// <param name="task">The task.</param>
        public void AddTask(Entities.Task task)
        {
            var text = File.ReadAllText(this._filePath);
            var document = new XmlDocument();
            document.LoadXml(text);

            var elements = document.SelectSingleNode("Entities");
            var group = elements?.SelectSingleNode("Tasks");
            var element = document.CreateElement("Task");

            var id = document.CreateAttribute("Id");
            id.InnerText = task.Id.ToString(CultureInfo.InvariantCulture);
            element.Attributes.Append(id);

            var title = document.CreateAttribute("Title");
            title.InnerText = task.Title;
            element.Attributes.Append(title);

            var categoryId = document.CreateAttribute("CategoryId");
            categoryId.InnerText = task.CategoryId.ToString(CultureInfo.InvariantCulture);
            element.Attributes.Append(categoryId);
            
            var isCompleted = document.CreateAttribute("IsCompleted");
            isCompleted.InnerText = task.IsCompleted.ToString(CultureInfo.InvariantCulture);
            element.Attributes.Append(isCompleted);

            group?.AppendChild(element);

            using (var stringWriter = new StringWriter(CultureInfo.InvariantCulture))
            {
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    document.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    File.WriteAllText(this._filePath, stringWriter.GetStringBuilder().ToString());
                }
            }
        }

        /// <summary>
        /// Completes the task.
        /// </summary>
        /// <param name="task">The task.</param>
        public void UpdateTask(Entities.Task task)
        {
            var text = File.ReadAllText(this._filePath);
            var document = new XmlDocument();
            document.LoadXml(text);

            var elements = document.SelectSingleNode("Entities");
            var group = elements?.SelectSingleNode("Tasks");
            var items = group?.SelectNodes("Task");

            for (var i = 0; i < items?.Count; i++)
            {
                if (task.Id == int.Parse(items[i].Attributes["Id"].InnerText))
                {
                    items[i].Attributes["Title"].InnerText = task.Title;
                    items[i].Attributes["CategoryId"].InnerText = task.CategoryId.ToString();
                    items[i].Attributes["IsCompleted"].InnerText = task.IsCompleted.ToString();
                }
            }

            using (var stringWriter = new StringWriter(CultureInfo.InvariantCulture))
            {
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    document.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    File.WriteAllText(this._filePath, stringWriter.GetStringBuilder().ToString());
                }
            }
        }

        /// <summary>
        /// Deletes all categories.
        /// </summary>
        public void DeleteAllCategories()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Deletes all tasks.
        /// </summary>
        public void DeleteAllTasks()
        {
            throw new System.NotImplementedException();
        }
    }
}
