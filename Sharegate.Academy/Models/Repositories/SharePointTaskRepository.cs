﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.SharePoint.Client;

using Sharegate.Academy.Models.Entities;
using Sharegate.Academy.Options;

namespace Sharegate.Academy.Models.Repositories
{
    public class SharePointTaskRepository : ITaskRepository
    {
        private const string TitleField = "Title";
        private const string CategoryField = "Category";
        private const string IsCompletedField = "Is_x0020_Completed";
        private const string CategoriesListTitle = "AcademyCategories";
        private const string TasksListTitle = "AcademyTasks";
        private readonly IOptionsService _optionsService;

        public SharePointTaskRepository(IOptionsService optionsService)
        {
            this._optionsService = optionsService;
        }

        private string WebUrl => this._optionsService.CurrentOptions.WebUrl;
        
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns>The collection of categories.</returns>
        public IEnumerable<Category> GetCategories()
        {
            using (var context = new ClientContext(this.WebUrl))
            {
                var categoriesList = context.Web.Lists.GetByTitle(CategoriesListTitle);
                var categoriesItems = GetListItems(categoriesList);
                
                context.Load(categoriesItems);
                context.ExecuteQuery();

                foreach (var categoryItem in categoriesItems)
                {
                    yield return new Category(categoryItem[TitleField].ToString()) { Id = categoryItem.Id };
                }
            }
        }

        /// <summary>
        /// Adds the category to the XML node list.
        /// </summary>
        /// <param name="category">The category.</param>
        public void AddCategory(Category category)
        {
            using (var context = new ClientContext(this.WebUrl))
            {
                var categoriesList = context.Web.Lists.GetByTitle(CategoriesListTitle);

                var newItem = categoriesList.AddItem(new ListItemCreationInformation());
                newItem[TitleField] = category.Title;

                newItem.Update();
                    
                context.Load(newItem);
                context.ExecuteQuery();

                category.Id = newItem.Id;
            }
        }

        /// <summary>
        /// Gets the tasks.
        /// </summary>
        /// <returns>The collection of tasks.</returns>
        public IEnumerable<Entities.Task> GetTasks()
        {
            using (var context = new ClientContext(this.WebUrl))
            {
                var tasksList = context.Web.Lists.GetByTitle(TasksListTitle);
                var taskItems = GetListItems(tasksList);
                
                context.Load(taskItems);
                context.ExecuteQuery();

                foreach (var taskItem in taskItems)
                {
                    var taskTitle = taskItem[TitleField].ToString();
                    var categoryId = ((FieldLookupValue)taskItem[CategoryField]).LookupId;
                    var isCompleted = (bool)taskItem[IsCompletedField];
                    yield return new Entities.Task(taskItem.Id, taskTitle, categoryId)
                                     {
                                         IsCompleted = isCompleted,
                                     };
                }
            }
        }

        /// <summary>
        /// Adds the task.
        /// </summary>
        /// <param name="task">The task.</param>
        public void AddTask(Entities.Task task)
        {
            using (var context = new ClientContext(this.WebUrl))
            {
                var tasksList = context.Web.Lists.GetByTitle(TasksListTitle);

                var newItem = tasksList.AddItem(new ListItemCreationInformation());
                newItem[TitleField] = task.Title;
                newItem[CategoryField] = new FieldLookupValue { LookupId = task.CategoryId, };
                newItem[IsCompletedField] = task.IsCompleted;

                newItem.Update();
                    
                context.Load(newItem);
                context.ExecuteQuery();

                task.Id = newItem.Id;
            }
        }

        /// <summary>
        /// Completes the task.
        /// </summary>
        /// <param name="task">The task.</param>
        public void UpdateTask(Entities.Task task)
        {
            using (var context = new ClientContext(this.WebUrl))
            {
                var tasksList = context.Web.Lists.GetByTitle(TasksListTitle);

                var existingItem = tasksList.GetItemById(task.Id);
                existingItem[TitleField] = task.Title;
                existingItem[CategoryField] = new FieldLookupValue { LookupId = task.CategoryId, };
                existingItem[IsCompletedField] = task.IsCompleted;

                existingItem.Update();
                
                context.ExecuteQuery();
            }
        }

        /// <summary>
        /// Deletes all categories.
        /// </summary>
        public void DeleteAllCategories()
        {
            using (var context = new ClientContext(this.WebUrl))
            {
                var categoriesList = context.Web.Lists.GetByTitle(CategoriesListTitle);
                var categoriesItems = GetListItems(categoriesList);
                
                context.Load(categoriesItems);
                context.ExecuteQuery();

                while (categoriesItems.Count > 0)
                {
                    categoriesItems.First().DeleteObject();
                }
                
                context.ExecuteQuery();
            }
        }

        /// <summary>
        /// Deletes all tasks.
        /// </summary>
        public void DeleteAllTasks()
        {
            using (var context = new ClientContext(this.WebUrl))
            {
                var tasksList = context.Web.Lists.GetByTitle(TasksListTitle);
                var taskItems = GetListItems(tasksList);
                
                context.Load(taskItems);
                context.ExecuteQuery();
                
                while (taskItems.Count > 0)
                {
                    taskItems.First().DeleteObject();
                }
                
                context.ExecuteQuery();
            }
        }

        private static ListItemCollection GetListItems(List list)
        {
            var query = new CamlQuery();
            query.ViewXml = "<View Scope=\"RecursiveAll\"></View>";

            return list.GetItems(query);
        }
    }
}
