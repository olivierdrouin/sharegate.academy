﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Prism.Commands;
using Prism.Mvvm;
using Sharegate.Academy.Async;
using Sharegate.Academy.Models.Repositories;
using Sharegate.Academy.Popup;

namespace Sharegate.Academy.Task
{
    /// <summary>
    /// The task view model.
    /// </summary>
    public class TaskViewModel : BindableBase
    {
        #region Fields

        private readonly ITaskRepository _taskRepository;
        private readonly IAsyncHelper _asyncHelper;
        private readonly IPromptController _promptController;

        private bool _isLoading;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskViewModel" /> class.
        /// </summary>
        /// <param name="taskRepository">The task repository.</param>
        /// <param name="asyncHelper">The async helper.</param>
        /// <param name="promptController">The prompt controller.</param>
        public TaskViewModel(ITaskRepository taskRepository, IAsyncHelper asyncHelper, IPromptController promptController)
        {
            this._taskRepository = taskRepository;
            this._asyncHelper = asyncHelper;
            this._promptController = promptController;

            this.Tasks = new ObservableCollection<TaskEdit>();
            this.AddTaskCommand = new DelegateCommand(this.AddTask, this.CanAddTask);
            this.NewTask = TaskEdit.New();
            this.NewTask.CategoryId = -1;
            this.NewTask.PropertyChanged += this.NewTask_PropertyChanged;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the tasks.
        /// </summary>
        public ObservableCollection<TaskEdit> Tasks { get; }

        /// <summary>
        /// Gets the add task command.
        /// </summary>
        public DelegateCommand AddTaskCommand { get; }

        /// <summary>
        /// Gets the new task.
        /// </summary>
        public TaskEdit NewTask { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading.
        /// </summary>
        public bool IsLoading
        {
            get { return this._isLoading; }
            set { this.SetProperty(ref this._isLoading, value); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the specified category identifier.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        public void Initialize(int categoryId)
        {
            this.NewTask.CategoryId = categoryId;
            this.AddTaskCommand.RaiseCanExecuteChanged();

            this.SafeExecuteAsync(
                    () =>
                    {
                        return this._taskRepository.GetTasks().Where(e => e.CategoryId == categoryId).ToList();
                    },
                    x =>
                    {
                        this.Tasks.Clear();
                        foreach (var task in x.Result)
                        {
                            var edit = TaskEdit.FromEntity(task);
                            edit.PropertyChanged += this.OnTaskEditPropertyChanged;
                            this.Tasks.Add(edit);
                        }

                        this.IsLoading = false;
                        this.AddTaskCommand.RaiseCanExecuteChanged();
                    });
        }

        private void AddTask()
        {
            this.SafeExecuteAsync(
                () => this._taskRepository.AddTask(this.NewTask.ToEntity()), 
                x =>
                {
                    this.Initialize(this.NewTask.CategoryId);
                    this.NewTask.Title = string.Empty;
                });
        }

        private bool CanAddTask()
        {
            return !this.IsLoading && this.NewTask.IsValid;
        }

        private void OnTaskEditPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            var taskEdit = sender as TaskEdit;
            if (taskEdit != null)
            {
                this.SafeExecuteAsync(() => this._taskRepository.UpdateTask(taskEdit.ToEntity()));
            }
        }

        private void NewTask_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsValid")
            {
                this.AddTaskCommand.RaiseCanExecuteChanged();
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "We want to catch all exceptions.")]
        private void SafeExecuteAsync<T>(Func<T> operation, Action<AsyncResult<T>> callback = null)
        { 
            this.IsLoading = true;

            try
            {
                this._asyncHelper.Run(operation, callback);
            }
            catch (Exception ex)
            {
                this._promptController.ShowError(ex);
            }
            finally
            {
                this.IsLoading = false;
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "We want to catch all exceptions.")]
        private void SafeExecuteAsync(Action operation, Action<AsyncResult> callback = null)
        {
            this.IsLoading = true;

            try
            {
                this._asyncHelper.Run(operation, callback);
            }
            catch (Exception ex)
            {
                this._promptController.ShowError(ex);
            }
            finally
            {
                this.IsLoading = false;
            }
        }

        #endregion
    }
}
