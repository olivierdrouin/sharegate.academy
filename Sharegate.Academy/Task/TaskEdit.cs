﻿using System;

using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;

namespace Sharegate.Academy.Task
{
    /// <summary>
    /// The business object to edit a task.
    /// </summary>
    [Serializable]
    public class TaskEdit : BusinessBase<TaskEdit>
    {
        #region Fields

        /// <summary>
        /// The title property
        /// </summary>
        public static readonly PropertyInfo<string> TitleProperty = RegisterProperty<string>(x => x.Title);

        /// <summary>
        /// The category identifier property
        /// </summary>
        public static readonly PropertyInfo<int> CategoryIdProperty = RegisterProperty<int>(x => x.CategoryId);

        /// <summary>
        /// The identifier property
        /// </summary>
        public static readonly PropertyInfo<int> IdProperty = RegisterProperty<int>(x => x.Id);
        
        public static readonly PropertyInfo<bool> IsCompletedProperty = RegisterProperty<bool>(x => x.IsCompleted);

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetProperty(TitleProperty);
            }

            set
            {
                this.SetProperty(TitleProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the category identifier.
        /// </summary>
        public int CategoryId
        {
            get
            {
                return this.GetProperty(CategoryIdProperty);
            }

            set
            {
                this.SetProperty(CategoryIdProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id
        {
            get
            {
                return this.GetProperty(IdProperty);
            }

            set
            {
                this.SetProperty(IdProperty, value);
            }
        }
        
        public bool IsCompleted
        {
            get
            {
                return this.GetProperty(IsCompletedProperty);
            }

            set
            {
                this.SetProperty(IsCompletedProperty, value);
            }
        }

        #endregion

        #region Static Methods

        #endregion

        /// <summary>
        /// Factory method to create a new instance.
        /// </summary>
        /// <returns>Returns a new instance.</returns>
        public static TaskEdit New()
        {
            return DataPortal.Create<TaskEdit>();
        }

        /// <summary>
        /// Factory method to create a new instance from an entity.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <returns>New instance from entity</returns>
        public static TaskEdit FromEntity(Models.Entities.Task task)
        {
            var edit = TaskEdit.New();
            edit.Title = task.Title;
            edit.CategoryId = task.CategoryId;
            edit.Id = task.Id;
            edit.IsCompleted = task.IsCompleted;

            return edit;
        }

        #region Methods

        /// <summary>
        /// Created an entity from the edit.
        /// </summary>
        /// <returns>A task entity</returns>
        public Models.Entities.Task ToEntity()
        {
            return new Models.Entities.Task() { CategoryId = this.CategoryId, Title = this.Title, Id = this.Id, IsCompleted = this.IsCompleted };
        }

        /// <summary>
        /// DataPortal method to create an instance.
        /// </summary>
        [RunLocal]
        protected override void DataPortal_Create()
        {
            base.DataPortal_Create();
        }

        /// <summary>
        /// Adds the business rules.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            this.BusinessRules.AddRule(new Lambda(TitleProperty, ValidateTitle));
            this.BusinessRules.AddRule(new MinValue<int>(CategoryIdProperty, 0));
        }
        
        private static void ValidateTitle(RuleContext context)
        {
            var task = (TaskEdit)context.Target;
            
            if (string.IsNullOrWhiteSpace(task.Title))
            {
                context.AddErrorResult(TitleProperty, "Title cannot be empty.");
            }

            if (task.Title.StartsWith(" "))
            {
                context.AddErrorResult(TitleProperty, "Remove spaces before the task title.");
            }
            
            if (task.Title.EndsWith(" "))
            {
                context.AddErrorResult(TitleProperty, "Remove spaces after the task title.");
            }
        }

        #endregion
    }
}
