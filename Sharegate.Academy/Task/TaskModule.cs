﻿using Microsoft.Practices.Unity;

using Prism.Modularity;
using Prism.Regions;

namespace Sharegate.Academy.Task
{
    /// <summary>
    /// The module responsible for handling todos.
    /// </summary>
    /// <seealso cref="Prism.Modularity.IModule" />
    public class TaskModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskModule"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="regionManager">The region manager.</param>
        public TaskModule(IUnityContainer container, IRegionManager regionManager)
        {
            this._container = container;
            this._regionManager = regionManager;
        }

        /// <summary>
        /// Notifies the module that it has be initialized.
        /// </summary>
        public void Initialize()
        {
            var region = this._regionManager.Regions[RegionNames.Content];
            region.Add(this._container.Resolve<TaskView>());
        }
    }
}
