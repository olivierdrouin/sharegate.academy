﻿using System.Windows.Controls;

namespace Sharegate.Academy.Task
{
    /// <summary>
    /// Interaction logic for TaskView.xaml
    /// </summary>
    public partial class TaskView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskView"/> class.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        public TaskView(TaskViewModel viewModel)
        {
            this.ViewModel = viewModel;
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        public TaskViewModel ViewModel
        {
            get { return (TaskViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }
    }
}
