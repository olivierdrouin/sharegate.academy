﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sharegate.Academy
{
    /// <summary>
    /// Defines the name of the Prism regions used by the applications.
    /// </summary>
    public static class RegionNames
    {
        /// <summary>
        /// The name of the content region.
        /// </summary>
        public const string Content = "ContentRegion";

        /// <summary>
        /// The name of the navigation region.
        /// </summary>
        public const string Navigation = "NavigationRegion";

        /// <summary>
        /// The name of the popup region.
        /// </summary>
        public const string Popup = "PopupRegion";
    }
} 
