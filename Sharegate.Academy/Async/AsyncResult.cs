﻿using System;

namespace Sharegate.Academy.Async
{
    /// <summary>
    /// The result of an async operation.
    /// </summary>
    public class AsyncResult
    {
        #region Constructors

        /// <summary>
        /// Creates a new instance of the AsyncResult class.
        /// </summary>
        public AsyncResult()
        {
            this.Success = true;
        }

        /// <summary>
        /// Creates a new instance of the AsyncResult class.
        /// </summary>
        /// <param name="ex">The exception that occurred during the async operation.</param>
        public AsyncResult(Exception ex)
        {
            this.Exception = ex;
            this.Success = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The exception that occurred during the async operation.
        /// </summary>
        public Exception Exception { get; protected set; }

        /// <summary>
        /// Gets if the async operation was a success.
        /// </summary>
        public bool Success { get; protected set; }

        #endregion
    }
}
