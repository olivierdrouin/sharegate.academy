﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace Sharegate.Academy.Async
{
    /// <summary>
    /// A helper class for executing asynchronous operations.
    /// </summary>
    public class AsyncHelper : IAsyncHelper
    {
        #region Fields

        private bool _disposed;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncHelper"/> class.
        /// </summary>
        public AsyncHelper()
        {
            if (Application.Current != null && Application.Current.Dispatcher != null && Application.Current.Dispatcher.Thread != null)
            {
                this.DesiredCulture = Application.Current.Dispatcher.Thread.CurrentCulture;
            }
            else
            {
                this.DesiredCulture = Thread.CurrentThread.CurrentCulture;
            }

            // use the application dispatcher if one is available
            if (Application.Current != null && Application.Current.Dispatcher != null)
            {
                this.CurrentDispatcher = Application.Current.Dispatcher;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IAsyncHelper"/> is disabled.
        /// </summary>
        public static bool Disabled { get; set; }

        /// <summary>
        /// Gets the culture to set on the worker thread.
        /// </summary>
        internal CultureInfo DesiredCulture { get; set; }

        /// <summary>
        /// Gets the dispatcher for the current thread.
        /// </summary>
        internal Dispatcher CurrentDispatcher { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Run a delegate on a background thread.
        /// </summary>
        /// <param name="work">The work delegate.</param>
        public void Run(Action work)
        {
            this.Run(work, null);
        }

        /// <summary>
        /// Run a delegate on a background thread then execute the result on the callback dispatcher's thread.
        /// </summary>
        /// <param name="work">The work delegate.</param>
        /// <param name="complete">The completed delegate.</param>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Exception is marshaled")]
        public void Run(Action work, Action<AsyncResult> complete)
        {
            // wrap the complete handler and execute using a null "object" return value
            Action<AsyncResult<object>> wrappedComplete = complete;
            this.Run(
                () =>
                {
                    work();
                    return null;
                },
                wrappedComplete);
        }

        /// <summary>
        /// Run a delegate on a background thread.
        /// </summary>
        /// <typeparam name="T">The result type of the work delegate.</typeparam>
        /// <param name="work">The work delegate.</param>
        public void Run<T>(Func<T> work)
        {
            this.Run(work, null);
        }

        /// <summary>
        /// Run a delegate on a background thread then execute the result on the callback dispatcher's thread.
        /// </summary>
        /// <typeparam name="T">The result type of the work delegate.</typeparam>
        /// <param name="work">The work delegate.</param>
        /// <param name="complete">The completed delegate.</param>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Exception is marshaled")]
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Type inferrence will handle this properly")]
        public void Run<T>(Func<T> work, Action<AsyncResult<T>> complete)
        {
            if (this._disposed)
            {
                return;
            }

            var dispatcher = this.CurrentDispatcher;

            WaitCallback backgroundAction = x =>
            {
                AsyncResult<T> result;

                try
                {
                    result = new AsyncResult<T>(work());
                }
                catch (Exception ex)
                {
                    result = new AsyncResult<T>(ex);
                }

                if (complete != null && !this._disposed)
                {
                    // if there was a dispatcher on the calling thread, send back to that dispatcher
                    if (dispatcher != null)
                    {
                        dispatcher.BeginInvoke(complete, result);
                    }
                    else
                    {
                        complete(result);
                    }
                }
            };

            this.Execute(backgroundAction);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            this._disposed = true;
        }

        private void Execute(WaitCallback backgroundAction)
        {
            if (AsyncHelper.Disabled)
            {
                backgroundAction(null);
            }
            else
            {
                ThreadPool.QueueUserWorkItem(x =>
                {
                    // set the thread culture to the same as the Application so that localized messages on the background
                    // thread will have the same culture
                    Thread.CurrentThread.CurrentCulture = this.DesiredCulture;
                    Thread.CurrentThread.CurrentUICulture = this.DesiredCulture;

                    backgroundAction(x);
                });
            }
        }

        #endregion
    }
}
