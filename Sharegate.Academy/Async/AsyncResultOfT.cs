﻿using System;

namespace Sharegate.Academy.Async
{
    /// <summary>
    /// The result of an async operation.
    /// </summary>
    /// <typeparam name="T">The type of the result.</typeparam>
    public class AsyncResult<T> : AsyncResult
    {
        #region Constructors

        /// <summary>
        /// Creates a new instance of the AsyncResult class.
        /// </summary>
        /// <param name="result">The result of the async operation.</param>
        public AsyncResult(T result)
            : base()
        {
            this.Result = result;
        }

        /// <summary>
        /// Creates a new instance of the AsyncResult class.
        /// </summary>
        /// <param name="ex">The exception that occurred during the async operation.</param>
        public AsyncResult(Exception ex)
            : base(ex)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// The result of the async operation.
        /// </summary>
        public T Result
        {
            get;
            protected set;
        }

        #endregion
    }
}
