﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Remoting.Messaging;

namespace Sharegate.Academy.Async
{
    /// <summary>
    /// Interface for the AsyncHelper.
    /// </summary>
    public interface IAsyncHelper : IDisposable
    {
        /// <summary>
        /// Run a delegate on a background thread.
        /// </summary>
        /// <param name="work">The work delegate.</param>
        void Run(Action work);

        /// <summary>
        /// Run a delegate on a background thread then execute the result on the callback dispatcher's thread.
        /// </summary>
        /// <param name="work">The work delegate.</param>
        /// <param name="complete">The completed delegate.</param>
        void Run(Action work, Action<AsyncResult> complete);

        /// <summary>
        /// Run a delegate on a background thread.
        /// </summary>
        /// <typeparam name="T">The result type of the work delegate.</typeparam>
        /// <param name="work">The work delegate.</param>
        void Run<T>(Func<T> work);

        /// <summary>
        /// Run a delegate on a background thread then execute the result on the callback dispatcher's thread.
        /// </summary>
        /// <typeparam name="T">The result type of the work delegate.</typeparam>
        /// <param name="work">The work delegate.</param>
        /// <param name="complete">The completed delegate.</param>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Type inferrence will be used for this method.")]
        void Run<T>(Func<T> work, Action<AsyncResult<T>> complete);
    }
}
