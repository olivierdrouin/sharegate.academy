﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sharegate.Academy
{
    /// <summary>
    /// Defines the types of repositories supported by the application.
    /// </summary>
    public enum RepositoryType
    {
        /// <summary>
        /// Tasks and categories will be stored in the local file system.
        /// </summary>
        Filesystem,

        /// <summary>
        /// Tasks and categories will be stored in SharePoint.
        /// </summary>
        SharePoint
    }
}
