﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Unity;
using Sharegate.Academy.Async;
using Sharegate.Academy.Models.Repositories;
using Sharegate.Academy.Navigation;
using Sharegate.Academy.Options;
using Sharegate.Academy.Popup;
using Sharegate.Academy.Task;

namespace Sharegate.Academy
{
    /// <summary>
    /// The bootstrapper class for the application.
    /// </summary>
    /// <seealso cref="UnityBootstrapper" />
    public class Bootstrapper : UnityBootstrapper
    {
        public static void RegisterTaskRepository(IUnityContainer container, RepositoryType repositoryType)
        {
            if (repositoryType == RepositoryType.SharePoint)
            {
                container.RegisterType<ITaskRepository, SharePointTaskRepository>();
            }
            else
            {
                container.RegisterType<ITaskRepository, XmlTaskRepository>();
            }
        }
        
        /// <summary>
        /// Creates the shell or main window of the application.
        /// </summary>
        /// <returns>The shell of the application.</returns>
        /// <remarks>
        /// If the returned instance is a <see cref="T:System.Windows.DependencyObject" />, the
        /// <see cref="T:Prism.Bootstrapper" /> will attach the default <see cref="T:Prism.Regions.IRegionManager" /> of
        /// the application in its <see cref="F:Prism.Regions.RegionManager.RegionManagerProperty" /> attached property
        /// in order to be able to add regions by using the <see cref="F:Prism.Regions.RegionManager.RegionNameProperty" />
        /// attached property from XAML.
        /// </remarks>
        protected override DependencyObject CreateShell()
        {
            return this.Container.TryResolve<Shell>();
        }

        /// <summary>
        /// Initializes the shell.
        /// </summary>
        protected override void InitializeShell()
        {
            base.InitializeShell();

            if (App.Current != null)
            {
                App.Current.MainWindow = (Window)this.Shell;
            }
        }

        /// <summary>
        /// Configures the <see cref="T:Prism.Modularity.IModuleCatalog" /> used by Prism.
        /// </summary>
        protected override void ConfigureModuleCatalog()
        {
            this.RegisterModule<TaskModule>();
            this.RegisterModule<NavigationModule>();
            this.RegisterModule<OptionsModule>();

            base.ConfigureModuleCatalog();
        }

        /// <summary>
        /// Configures the <see cref="T:Microsoft.Practices.Unity.IUnityContainer" />. May be overwritten in a derived class to add specific
        /// type mappings required by the application.
        /// </summary>
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            this.Container.RegisterType<IAsyncHelper, AsyncHelper>();
            this.Container.RegisterType<INavigationController, NavigationController>();
            this.Container.RegisterType<IOptionsService, OptionsService>();
            var optionsService = this.Container.Resolve<IOptionsService>();
            RegisterTaskRepository(this.Container, optionsService.CurrentOptions.RepositoryType);
            
            this.Container.RegisterType<IPromptController, PromptController>();
        }

        private void RegisterModule<TModule>() where TModule : class, IModule
        {
            var moduleType = typeof(TModule);

            // The module name should use AssemblyQualifiedName and not only Name to avoid duplicate module name after obfuscation
            var moduleInfo = new ModuleInfo(moduleType.AssemblyQualifiedName, moduleType.AssemblyQualifiedName)
            {
                InitializationMode = InitializationMode.WhenAvailable,
            };

            this.ModuleCatalog.AddModule(moduleInfo);
        }
    }
}
